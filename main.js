let btnsList = document.querySelector(".btn-wrapper");

window.addEventListener("keypress", function (event) {
  let btn = event.charCode;
  switch (btn) {
    case 115:
      if (this.document.querySelector(".pressed")) {
        this.document.querySelector(".pressed").classList.remove("pressed");
      }
      btnsList.children[1].classList.add("pressed");

      break;
    case 101:
      if (this.document.querySelector(".pressed")) {
        this.document.querySelector(".pressed").classList.remove("pressed");
      }
      btnsList.children[2].classList.add("pressed");

      break;
    case 111:
      if (this.document.querySelector(".pressed")) {
        this.document.querySelector(".pressed").classList.remove("pressed");
      }
      btnsList.children[3].classList.add("pressed");

      break;
    case 110:
      if (this.document.querySelector(".pressed")) {
        this.document.querySelector(".pressed").classList.remove("pressed");
      }
      btnsList.children[4].classList.add("pressed");

      break;
    case 108:
      if (this.document.querySelector(".pressed")) {
        this.document.querySelector(".pressed").classList.remove("pressed");
      }
      btnsList.children[5].classList.add("pressed");

      break;
    case 122:
      if (this.document.querySelector(".pressed")) {
        this.document.querySelector(".pressed").classList.remove("pressed");
      }
      btnsList.children[6].classList.add("pressed");

      break;
    case 13:
      if (this.document.querySelector(".pressed")) {
        this.document.querySelector(".pressed").classList.remove("pressed");
      }
      btnsList.children[0].classList.add("pressed");

      break;
  }
});
